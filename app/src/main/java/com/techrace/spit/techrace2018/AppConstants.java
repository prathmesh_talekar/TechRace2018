package com.techrace.spit.techrace2018;

public class AppConstants {

    public static final String password = "valarmorghulis";

    public static final String techRacePref = "com.techrace.spit.techrace2018";
    public static final String hintsLeftPref = "Hints Left";
    public static final String hintPref = "Hint";
    public static final String cooldownPref = "Cooldown";
    public static final String levelPref = "Level";
    public static final String pointsPref = "Points";
    public static final String cluePref = "Clue";
    public static final String clueLevelPref = "Clue ";
    public static final String locationLevelPref = "Location ";

    public static final int plusTwoPrice = 20;
    public static final int plusFourPrice = 30;
    public static final int unlockACluePrice = 40;
    public static final int reversePrice = 35;
    public static final int hint1Price = 0;
    public static final int hint2Price = 10;
    public static final int hint3Price = 20;
    public static final int jackpotPrice = 50;

//    public static final int loc1level1 = 5;
//    public static final int loc1level2 = 5;
//    public static final int loc1level3 = 5;
//    public static final int loc2level1 = 5;
//    public static final int loc2level2 = 5;
//    public static final int loc2level3 = 5;
//    public static final int loc3level1 = 5;
//    public static final int loc3level2 = 5;
//    public static final int loc3level3 = 5;
//    public static final int loc4level1 = 5;
//    public static final int loc4level2 = 5;
//    public static final int loc4level3 = 5;
//    public static final int loc5level1 = 5;
//    public static final int loc5level2 = 5;
//    public static final int loc5level3 = 5;
//    public static final int loc6level1 = 5;


}
