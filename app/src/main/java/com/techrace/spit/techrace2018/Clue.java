package com.techrace.spit.techrace2018;

public class Clue {
    String mClue;
    public Clue(String clue){
        mClue=clue;
    }

    public String getmClue() {
        return mClue;
    }

    public void setmClue(String mClue) {
        this.mClue = mClue;
    }
}
